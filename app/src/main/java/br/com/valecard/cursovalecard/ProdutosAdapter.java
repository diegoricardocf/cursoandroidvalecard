package br.com.valecard.cursovalecard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProdutosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ProdutoValeCard> listaItens;
    private LayoutInflater mInflater;
    private Context mContext;

    ProdutosAdapter(Context context, List<ProdutoValeCard> list) {
        mContext = context;
        listaItens = list;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getItemCount() {
        return listaItens.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MeuViewHolder(mInflater.inflate(R.layout.item_lista_produtos, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MeuViewHolder viewHolder = (MeuViewHolder) holder;
        final ProdutoValeCard item = listaItens.get(position);

        viewHolder.descricaoDoProduto.setText(item.getNome());
        viewHolder.imagemDoProduto.setImageResource(item.getIdImagem());

        viewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Cliquei em " + item.getNome(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private class MeuViewHolder extends RecyclerView.ViewHolder {

        ImageView imagemDoProduto;
        TextView  descricaoDoProduto;
        RelativeLayout item;
        MeuViewHolder(View view) {
            super(view);
            imagemDoProduto = view.findViewById(R.id.imagem_produto);
            descricaoDoProduto = view.findViewById(R.id.descricao_produto);
            item = view.findViewById(R.id.item_produto);
        }
    }
}
