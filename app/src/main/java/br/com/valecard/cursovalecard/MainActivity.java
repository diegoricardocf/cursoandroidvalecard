package br.com.valecard.cursovalecard;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Hawk.init(this).build();
        setListeners();
        veryfyUserName();
    }

    private void veryfyUserName() {
        if(Hawk.contains("userName")){
            String name = Hawk.get("userName");
            Toast.makeText(this, "Seja bem vindo "+ name, Toast.LENGTH_SHORT).show();
        }
        else{
            Intent intencao = new Intent(this,UserNameActivity.class);
            startActivity(intencao);
        }
    }

    private void setListeners() {
        RelativeLayout layoutProdutos = findViewById(R.id.produtos_relative_layout);

        layoutProdutos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,getString(R.string.click_product), Toast.LENGTH_LONG).show();
                Intent intent =  new Intent(MainActivity.this,ProdutosActivity.class);
                startActivity(intent);
            }
        });

        RelativeLayout layoutVisao = findViewById(R.id.visaoLayout);

        layoutVisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Cliquei na view de visao", Toast.LENGTH_LONG).show();
            }
        });
        RelativeLayout layoutHorario = findViewById(R.id.horarioLayout);

        layoutHorario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Cliquei na view de horario", Toast.LENGTH_LONG).show();
            }
        });
        RelativeLayout layoutContatos = findViewById(R.id.contatoLayout);

        layoutContatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Cliquei na view de contato", Toast.LENGTH_LONG).show();
            }
        });
    }


}
