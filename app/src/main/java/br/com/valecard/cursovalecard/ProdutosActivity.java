package br.com.valecard.cursovalecard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class ProdutosActivity extends AppCompatActivity {

    List<ProdutoValeCard> produtos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtos);
        populaListaProdutos();
        configuraRecyclerView();

    }

    private void configuraRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recyclerViewProdutos);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        ProdutosAdapter adapter = new ProdutosAdapter(this,produtos);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void populaListaProdutos() {
        ProdutoValeCard p1 =  new ProdutoValeCard("Refeição",R.drawable.ic_pin_refeicao);
        ProdutoValeCard p2 =  new ProdutoValeCard("Alimentação",R.drawable.pin_alimentacao);
        ProdutoValeCard p3 =  new ProdutoValeCard("Combustível",R.drawable.ic_pin_combustivel);
        ProdutoValeCard p4 =  new ProdutoValeCard("Convenio",R.drawable.ic_pin_convenio);

        produtos.add(p1);
        produtos.add(p2);
        produtos.add(p3);
        produtos.add(p4);

    }

}
