package br.com.valecard.cursovalecard;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class UserNameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_name);
        Hawk.init(this).build();
        setListeners();
    }

    private void setListeners() {
        final EditText nameEditText = findViewById(R.id.userNameEditText);
        Button buttonSave = findViewById(R.id.buttonSaveUserName);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nameEditText.getText().toString().isEmpty()){
                    Toast.makeText(UserNameActivity.this, "Digite o nome cabeção", Toast.LENGTH_SHORT).show();
                }
                else{
                    String name = nameEditText.getText().toString();
                    Hawk.put("userName",name);
                    finish();
                }
            }
        });
    }
}
