package br.com.valecard.cursovalecard;

public class ProdutoValeCard {
    private String nome;
    private Integer idImagem;

    public ProdutoValeCard(String nome, Integer idImagem) {
        this.nome = nome;
        this.idImagem = idImagem;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdImagem() {
        return idImagem;
    }

    public void setIdImagem(Integer idImagem) {
        this.idImagem = idImagem;
    }
}
